send = require './send.coffee'

module.exports = (data) ->
  if data.msgType == 'carPositions'
    throttle = throttle_from_positions(data['data'])
    console.log(throttle)
    send {msgType: "throttle", data: throttle}
  else
    if data.msgType == 'join'
      console.log 'Joined'
    else if data.msgType == 'gameStart'
      console.log 'Race started'
    else if data.msgType == 'gameEnd'
      console.log 'Race ended'
    send {msgType: "ping", data: {}}

not_our_bot = (bot) ->
  bot['name'] != GLOBAL.botName

throttle_from_positions = (positions) ->
  for position in positions
    if not_our_bot(position['id'])
      console.log('Enemy targeted: ', position['id']['name'])
    else
      console.log('Our position is: ', position)

  Math.random()
