net        = require("net")
JSONStream = require('JSONStream')
main       = require './main.coffee'
send       = require './send.coffee'

serverHost = process.argv[2] or "testserver.helloworldopen.com"
serverPort = process.argv[3] or 8091
GLOBAL.botName = process.argv[4] or "WinTheory"
botKey     = process.argv[5] or "SBlQUWnWfEhZdw"

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort)

GLOBAL.client = net.connect serverPort, serverHost, () ->
  send({ msgType: "join", data: { name: botName, key: botKey }})

jsonStream = client.pipe(JSONStream.parse())

jsonStream.on 'data', (data) ->
  main(data)

jsonStream.on 'error', ->
  console.log "disconnected"
