## Hello World Open (HWO) 2014

It contains simple racing bot implementations in different languages.

- [Tech Specs](https://helloworldopen.com/techspec)

### Get started

Install:

    git clone git@bitbucket.org:hwo2014/hwo2014-team-1429.git
    cd hwo2014-team-1429/coffeescript

Run it:

    ./run

This will run our bot on the main test server

# Let's do it!